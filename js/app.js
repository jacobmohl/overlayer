var overlayWindow;

/*** URLS ***/
var regatteLeaderboardURL = "http://danishleague2014.sapsailing.com/sailingserver/leaderboard?leaderboardName=Danish League 2014 - Regatta 2 (J/70)";
var seriesLeaderboardUrl = "http://danishleague2014.sapsailing.com/sailingserver/leaderboard?leaderboardName=Danish League 2014 Overall";
var raceStatusUrl = "http://danishleague2014.sapsailing.com/sailingserver/eventRaceStates?eventId=2ecdcb80-e482-49e9-9c64-a8635c2a08d2&filterByDayOffset=0";

/*** GENERICS ***/
function updateElementWithValueInOverlay(element, value) {$(element).html(value); }
function toggleElement(element) {$(element).toggleClass("hide"); }
function hideElement(element) {$(element).addClass("hide"); }

function updateRaceTime(time) {
    $("#countdown").clock(time);
}

function initSettings() {
    $("#settings_hideAll").click(function () {
        overlayWindow.hideElement(".lowerthird");
        overlayWindow.hideElement(".upperleft");
        overlayWindow.hideElement(".race-time");
        overlayWindow.hideElement(".race-number");
    });
    
    $("#settings_openOverlay").click(function () {
        overlayWindow = window.open(
            "overlay.html?serverUrl="+$("#settings_serverURL").val(),
            "_blank"
        );
    });
}

/*** LOWER THIRD ***/
function initLowerThird() {
    $("#lt_text1").on('input', function () {
        overlayWindow.updateElementWithValueInOverlay(".lowerthird-small", $(this).val());
    });
    
    $("#lt_text2").on('input', function () {
        overlayWindow.updateElementWithValueInOverlay(".lowerthird-big", $(this).val());
    });
    
    $("#lt_toggle").click(function () {
        overlayWindow.toggleElement(".lowerthird");
    });
}

/*** UPPER LEFT ***/
function initUpperLeft() {
    $("#ul_text").on('input', function () {
        overlayWindow.updateElementWithValueInOverlay(".upperleft", $(this).val());
    });
    
    $("#ul_toggle").click(function () {
        overlayWindow.toggleElement(".upperleft");
    });
}




function getRunningRaceInfo() {
    //var url = 'racestatus.json'
    
    $.getJSON(raceStatusUrl, function (data) {
        
        var RaceStates = data.raceStates;
        RaceStates = data.raceStates;

        $.each(RaceStates, function (index, values) {
            
            var raceState, flight, fleetName, race, startTime;
            raceState = values.raceState;

            if (raceState.lastStatus !== 'FINISHED' && raceState.lastStatus !== 'UNSCHEDULED') {
                
                
                flight = values.raceName.substring(1, values.raceName.length);
                fleetName = values.fleetName;
                race = (flight - 1) * 3;
                
                if (fleetName === 'Blue') {
                    race += 1;
                } else if (fleetName === 'Green') {
                    race += 2;
                } else if (fleetName === 'Yellow') {
                    race += 3;
                }

                startTime = new Date(raceState.startTime);
                              
                overlayWindow.updateRaceTime(startTime);
                overlayWindow.updateElementWithValueInOverlay(".race-number-number", race);
            }
        });

    });
}


/*** RACE INFO ***/
function initRaceInfo() {
    
    // Update the racenumber
    $("#race-info_number").on('input', function () {
        overlayWindow.updateElementWithValueInOverlay(".race-number-number", $(this).val());
    });

    $("#race-info_update").click(function () {
        var now, custom_date;
        now = new Date();
        custom_date = new Date(now.getFullYear(), now.getMonth(), now.getDate(), $("#race-time_hour").val(), $("#race-time_minutes").val());
        overlayWindow.updateRaceTime(custom_date);
    });
    
    // Show and hide
    $("#race-time_toggle").click(function () {
        overlayWindow.toggleElement(".race-time");
    });
    
    $("#race-number_toggle").click(function () {
        overlayWindow.toggleElement(".race-number");
    });
    
    $("#race-info_toggle").click(function () {
        overlayWindow.toggleElement(".race-number");
        overlayWindow.toggleElement(".race-time");
    });
    
    $("#race-info_update_sap").click(function () {
        getRunningRaceInfo();
    });
}

function initLeaderboards() {
    $("#leaderboards-regatte_loader").click(function () {
        overlayWindow.getRegattaLeaderboard($("#leaderboards_type").val(), $("#leaderboards_pager").val());
    });

    $("#leaderboards-regatte_toggle").click(function () {
        overlayWindow.toggleElement(".leaderboard");
        overlayWindow.toggleElement(".upperleft-sap");
    });
}

    
$(document).ready(function () {
    initSettings();
    initRaceInfo();
    initLowerThird();
    initUpperLeft();
    initLeaderboards();

    /* Start the realtime leaderbord
    setInterval(function () {
        reloadRealtimeLeadeboard()
    }, 2000);
    */
    
});