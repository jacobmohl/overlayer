/// Countdown to the next event
(function ($) {
    var interval, days, hours, minutes, seconds, countdownDate;
    
    // Init
    $.fn.clock = function (date) {
        countdownDate = date;
        $.fn.clock.startClock(this);
        return this;
    };
    
    
    // Private methods
    function updateClockface(clockface, oldTime, newTime, element) {
        
        
        if (oldTime !== newTime) {
            oldTime = newTime;
            
            var valueElement = $(clockface, element);
            valueElement.html(oldTime);
        }
        return oldTime;
    }
    
    
      
    // Start the clock
    $.fn.clock.startClock = function (element) {
        interval = setInterval(function () {
            var timespan = countdown(countdownDate);
            
            window.console.log(countdownDate);
            
            seconds = updateClockface("#seconds", seconds, timespan.seconds, element);
            minutes = updateClockface("#minutes", minutes, timespan.minutes, element);
            hours = updateClockface("#hours", hours, timespan.hours, element);
            days = updateClockface("#days", days, timespan.days, element);
            
        }, 1000);
    };
    
    
    // TODO: Dosent work
    $.fn.clock.stopClock = function () {
        clearInterval(interval);
    };
    
    // TODO: Make call back when the countdown reach 0
    
    


}(jQuery));