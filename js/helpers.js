/* Hide an element */
function hideElement(element) {$(element).addClass("hide"); }

/* Update an element with an value */
function updateElementWithValueInOverlay(element, value) {$(element).html(value); }

/* Toggle between hide/show */
function toggleElement(element) {$(element).toggleClass("hide"); }


