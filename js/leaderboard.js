function getRegattaLeaderboard(type,pager) {
    var url, leaderboardName;

    if (type === "Regatta") {
        url = regattaLeaderboardUrl;
        leaderboardName = "Sejlsportsligaen | Hellerup";
    } else {
        url = seriesLeaderboardUrl;
        leaderboardName = "Sejlsportsligaen | Samlet stilling";
    }
    
    $.getJSON(url, function (data) {
		
        var h1, h2, Header = [], Row = [], Values = [], Competitors = [], toWrite = '', imgFlagPath = 'gfx/club_flags/', maxRaceColumnsToShow = 5, flight = 1;
        
        $(".upperleft-sap-header").html(leaderboardName);
        
        Competitors = data.competitors;
			
        $.each(Competitors, function (index, values) {
            Values = values.raceScores;

            var raceWithValues = 0;
            $.each(Values, function (indexPoints, valuesPoints) {
                if (valuesPoints.totalPoints !== null) {
                    raceWithValues++;
                }
            });
            if (raceWithValues > flight) {
                flight = raceWithValues;
            }
        });
			
        var startRaceColumn = flight - maxRaceColumnsToShow + 1;
        if(startRaceColumn < 1) {
            startRaceColumn = 1; 
        }
			
        h1 = data.name.toString();
        if(data.scoringComment) {
            h2 = data.scoringComment.toString();
        } else {
            h2 = '';
        }
        
        Header.push("#");
        Header.push("Sejlklub");
        for (var i = startRaceColumn; i <= flight; i++) {
            Header.push(data.columnNames[i-1]);
        }
        Header.push("&sum;");

        //create HTML Header
        toWrite += '<thead><tr>';
        for (i = 0; i < Header.length; i++) {
            toWrite += '<th>' + Header[i] + '</th>';
        }
        toWrite += '</tr></thead><tbody>';

        //collect Row Data
        $.each(Competitors, function (index, values) {
            if(pager === 1 ) {
                if (index > 8) {  
                    window.console.log("Nu er grænsen nået");
                    return true;
                }
            }
            
            if(pager === 2 ) {
                if (index < 9) {  
                    window.console.log("Nu er grænsen nået");
                    return true;
                }
            }            
            
            Row.push(values.rank.toString());
            Row.push(values.name.toString());
            Values = values.raceScores;

            //get Competitor Points
            var currentRaceColumnNumber = 1;
            $.each(Values, function (indexPoints, valuesPoints) {
                if(currentRaceColumnNumber >= startRaceColumn && currentRaceColumnNumber <= flight) {
                    if(valuesPoints.maxPointsReason.toString() !== 'NONE') {
                        Row.push(valuesPoints.maxPointsReason.toString());
                    } else {
                        if(valuesPoints.totalPoints !== null) {
                            Row.push(valuesPoints.totalPoints);
                        } else {
                            Row.push("");
                        }									
                    }
                }
                currentRaceColumnNumber++;
            });

            Row.push(values.totalPoints.toString());
            //Row complete

            //write Row
            toWrite += '<tr>';
            for (var j = 0; j < Header.length; j++) {
                if (j < Row.length) {
                    if(j === 0) {
                        toWrite += '<td class="nr">' + Row[j] + '</td>';
                    } else if(j === 1) {
                        toWrite += '<td>';
                        toWrite += '<img class="clubflag" src="' + imgFlagPath + values.sailID.toString().toLowerCase() + '.png">';
                        toWrite += '<span>' + Row[j] + '</span>';
                        toWrite += '</td>';
                    } else if (j > 1 && j < (Row.length - 1)) {
                        toWrite += '<td class="points">' + Row[j] + '</td>';
                    } else {
                        toWrite += '<td class="sum">' + Row[j] + '</td>';
                    }
                } else {//fill rest of the Header row
                    toWrite += '<td>&nbsp;</td>';
                }
            }

            toWrite += '</tr>';
            //Row completed

            //reset Row
            Row = [];
        });
        
        toWrite += '</tbody>';

//            $('#h1').text(h1);
        $('#h2').text(h2);
        $('#leaderboardTable').html(toWrite);
        $("#leaderboardTable").tablesorter().tablesorterPager({container: $("#pager")});
    });
}