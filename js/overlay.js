/*** HELPERS ***/

/* Get settings from openeer */
function getServerUrl(){ return window.opener.serverURL; }
function getLeaderboardNameOverall() { return window.opener.leaderboardNameOverall; }
function getLeaderboardNameRegatta() { return window.opener.leaderboardNameRegatta; }
function getEventId() { return window.opener.eventId; }

/*** 
***
URLS and vars
***
***/
var regattaLeaderboardUrl       = getServerUrl() + "sailingserver/leaderboard?leaderboardName=" + getLeaderboardNameRegatta();
var seriesLeaderboardUrl        = getServerUrl() + "sailingserver/leaderboard?leaderboardName=" + getLeaderboardNameOverall();
var raceStatusUrl               = getServerUrl() + "sailingserver/eventRaceStates?eventId=" + getEventId() + "&filterByDayOffset=0";
var liveRankingUrl              = getServerUrl() + "sailingserver/api/v1/regattas/" + getLeaderboardNameRegatta() + "/races/Race " + 1 + "/competitors/legs";


/*** 
****
RACEINFO 
***
***/

/* Update the racetime with new time infomations */
function updateRaceTime(time) {
    $("#countdown").clock(time);
}

/* Get information about the running race */
function getRunningRaceInfo() {
    //var url = 'racestatus.json'
    
    $.getJSON(raceStatusUrl, function (data) {
        
        var RaceStates = data.raceStates;
        RaceStates = data.raceStates;

        $.each(RaceStates, function (index, values) {
            
            var raceState, flight, fleetName, race, startTime;
            raceState = values.raceState;

            if (raceState.lastStatus !== 'FINISHED' && raceState.lastStatus !== 'UNSCHEDULED') {
                
                
                flight = values.raceName.substring(1, values.raceName.length);
                fleetName = values.fleetName;
                race = (flight - 1) * 3;
                
                if (fleetName === 'Blue') {
                    race += 1;
                } else if (fleetName === 'Green') {
                    race += 2;
                } else if (fleetName === 'Yellow') {
                    race += 3;
                }
                
                window.opener.updateRaceNumber(race);

                startTime = new Date(raceState.startTime);
                              
                updateRaceTime(startTime);
                updateElementWithValueInOverlay(".race-number-number", race);
            }
        });

    });
}


/*** RUNTIME ***/
$(document).ready(function () {
});