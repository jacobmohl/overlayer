function fillCompetitorItem(idNr, competitor) {
    
    // Telemetry
    var name, sailnumber, started, rank, speed, distanceTravleled, gapToLeader, jibes, penaltyCircles, tacks, maneuvers;
    name = competitor.name;
    sailnumber = competitor.sailnumber;
    started = competitor.started;
    rank = competitor.rank;
    speed = competitor['averageSOG-kts'];
    distanceTravleled = competitor['distanceTraveled-m'];
    gapToLeader = Math.floor(competitor['gapToLeader-s']);
    jibes = competitor.jibes;
    penaltyCircles = competitor.penaltyCircles;
    tacks = competitor.tacks;
    maneuvers = jibes + penaltyCircles + tacks;
    
    var competitorItem, itemHeight;
    competitorItem = $(".realtimeleaderboard #item-" + idNr);
    itemHeight = (rank - 1) * (33 + 5);

    if (started) {
        competitorItem.show();
    } else {
        competitorItem.hide();
    }
    
    competitorItem.css({ top: itemHeight+'px' });
    $(".plc", competitorItem).html(rank);
    $(".club", competitorItem).html(name);
    $(".telemetry", competitorItem).html(speed + " kts");
    //$(".telemetry", competitorItem).html(gapToLeader + " s");
    //$(".telemetry", competitorItem).html(distanceTravleled + " m");
}

function findActiveLeg(legs) {
    var i;
    var reverseLegs = legs.reverse();
    for (i = 0; i < reverseLegs.length; ++i) {
        
        var activeLeg = reverseLegs[i];
        
        var ii;
        for (ii = 0; ii < activeLeg.competitors.length; ++ii) {
            if(activeLeg.competitors[ii].started === true){
                return activeLeg;
            }
        }
        
        
    }
}



function reloadRealtimeLeadeboard(race) {
    window.console.log("Reload");
    
    var regatta, race, leg, url;
    regatta = "Bundesliga%20II%20-%202014%20(J70)";
    //race =  $("#raceNumber").val();//"25";
    url = liveRankingUrl; //"http://bundesliga2014.sapsailing.com/sailingserver/api/v1/regattas/" + regatta + "/races/Race%20" + race + "/competitors/legs";
    //leg = 4;
    
    $.getJSON(url, function (data){
        window.console.log(data);
        window.console.log("Data downloaded");
        
        var activeLeg = findActiveLeg(data.legs); //data.legs[leg-1];
        
        var i;        
        for (i = 0; i < activeLeg.competitors.length; ++i) {
            fillCompetitorItem(i+1,activeLeg.competitors[i]);
        }
        
    });
    
}