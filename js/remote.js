/** GLOBAL VARS **/
var serverURL, overlayWindow, leaderboardNameOverall, leaderboardNameRegatta, eventId, raceNumber;

/*** INIT FUNCTIONS ***/

/* Init settings */
function initSettings(){
    $("#settings_openOverlay").click(function () {
        overlayWindow = window.open(
            "overlay.html",
            "_blank",
            "height=1080,width=1920"
        );
    });   
    
    $("#settings_hideAll").click(function () {
        overlayWindow.hideElement(".lowerthird");
        overlayWindow.hideElement(".upperleft");
        overlayWindow.hideElement(".race-time");
        overlayWindow.hideElement(".race-number");
        overlayWindow.hideElement(".realtimeleaderboard");
    });
    
    
    // Set the global variables and change them when the input is changed
    serverURL = $("#settings_serverURL").val();
    leaderboardNameOverall = $("#settings_leaderboardOverall").val();
    leaderboardNameRegatta = $("#settings_leaderboardRegatta").val();
    eventId = $("#settings_eventId").val();
    
    $("#settings_serverURL").on('input', function () {              serverURL = $("#settings_serverURL").val(); });
    $("#settings_leaderboardOverall").on('input', function () {     leaderboardNameOverall = $("#settings_leaderboardOverall").val(); });
    $("#settings_leaderboardRegatta").on('input', function () {     leaderboardNameRegatta = $("#settings_leaderboardRegatta").val(); });
    $("#settings_eventId").on('input', function () {                eventId = $("#settings_eventId").val(); });

}

function updateRaceNumber(number){
    $("#settings_raceNumber").val(number);
    overlayWindow.updateElementWithValueInOverlay(".race-number-number", number);
    raceNumber = number;
}

/*** RACE INFO ***/
function initRaceInfo() {
    
    // Update the racenumber
    $("#settings_raceNumber").on('input', function () {
        overlayWindow.updateElementWithValueInOverlay(".race-number-number", $(this).val());
    });

    $("#race-info_update").click(function () {
        var now, custom_date;
        now = new Date();
        custom_date = new Date(now.getFullYear(), now.getMonth(), now.getDate(), $("#race-time_hour").val(), $("#race-time_minutes").val());
        overlayWindow.updateRaceTime(custom_date);
    });
    
    // Show and hide
    $("#race-time_toggle").click(function () {
        overlayWindow.toggleElement(".race-time");
    });
    
    $("#race-number_toggle").click(function () {
        overlayWindow.toggleElement(".race-number");
    });
    
    $("#race-info_toggle").click(function () {
        overlayWindow.toggleElement(".race-number");
        overlayWindow.toggleElement(".race-time");
    });
    
    $("#race-info_update_sap").click(function () {
        overlayWindow.getRunningRaceInfo();
    });
}

/* Ini live ranking */
function initLiveRanking(){
    $("#live-ranking_toggle").click(function () {
        overlayWindow.toggleElement(".realtimeleaderboard");
    });
                                    
    $("#live-ranking_update").click(function () {
        overlayWindow.reloadRealtimeLeadeboard(raceNumber);
    });
}

                                    


/* Init lower third ***/
function initLowerThird() {
    $("#lt_text1").on('input', function () {
        overlayWindow.updateElementWithValueInOverlay(".lowerthird-small", $(this).val());
    });
    
    $("#lt_text2").on('input', function () {
        overlayWindow.updateElementWithValueInOverlay(".lowerthird-big", $(this).val());
    });
    
    $("#lt_toggle").click(function () {
        overlayWindow.toggleElement(".lowerthird");
    });
}

/* Init upper left */
function initUpperLeft() {
    $("#ul_text").on('input', function () {
        overlayWindow.updateElementWithValueInOverlay(".upperleft", $(this).val());
    });
    
    $("#ul_toggle").click(function () {
        overlayWindow.toggleElement(".upperleft");
    });
}

function initLeaderboards() {
    $("#leaderboards-regatte_loader").click(function () {
        overlayWindow.getRegattaLeaderboard($("#leaderboards_type").val(), $("#leaderboards_pager").val());
    });

    $("#leaderboards-regatte_toggle").click(function () {
        overlayWindow.toggleElement(".leaderboard");
        overlayWindow.toggleElement(".upperleft-sap");
    });
}


$(document).ready(function () {
    initSettings();
    initLowerThird();
    initUpperLeft();
    initRaceInfo();
    initLeaderboards();
    initLiveRanking();
});